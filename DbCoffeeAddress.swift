//
//  DbCoffeeAddress+CoreDataClass.swift
//  DbTestSwift
//
//  Created by Рустам Мотыгуллин on 01.02.2021.
//
//

import Foundation
import CoreData

@objc(DbCoffeeAddress)
public class DbCoffeeAddress: NSManagedObject {
    
}

extension DbCoffeeAddress {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<DbCoffeeAddress> {
        return NSFetchRequest<DbCoffeeAddress>(entityName: "DbCoffeeAddress")
    }
    
    @NSManaged public var lat: Double
    @NSManaged public var long: Double
    @NSManaged public var street: String?
    @NSManaged public var time: String?
    @NSManaged public var coffee: DbCoffee?
    
}

extension DbCoffeeAddress : Identifiable {
    
}

//
//  CoffeeNew.swift
//  DbTestSwift
//
//  Created by Рустам Мотыгуллин on 01.02.2021.
//

import Foundation

struct CoffeeNew: Codable {
    let street: String
    let title: String
}

extension CoffeeNew {
    init(data: Data) throws {
        self = try JSONDecoder().decode(CoffeeNew.self, from: data)
    }
}

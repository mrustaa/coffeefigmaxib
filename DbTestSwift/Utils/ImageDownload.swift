//
//  FigmaDataClearFolder.swift
//  FigmaConvertXib
//
//  Created by Рустам Мотыгуллин on 25.08.2020.
//  Copyright © 2020 mrusta. All rights reserved.
//

import Foundation
import UIKit

class ImageDownload {
    
    class func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    class func downloadImage(urlStr: String, completion: (( _ data: Data, _ image: UIImage) -> Void)? = nil) {

        guard let url = URL(string: urlStr) else { return }
        
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            if let image = UIImage(data: data) {
                DispatchQueue.main.async() {
                    completion?(data, image)
                 }
            }
        }
    }
    
    
    class func downloadBdImage(urlStr: String, coffeeTitle: String, index: Int, completion: ((_ image: UIImage) -> Void)? = nil) {
        
        guard let url = URL(string: urlStr) else { return }
        
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            if let image = UIImage(data: data) {
                DispatchQueue.main.async() {
                    
                    if let coffee = CoreDataManager.getCoffeeTitle(title: coffeeTitle) {
                        switch index {
                        case 0: coffee.instaImg1 = data
                        case 1: coffee.instaImg2 = data
                        case 2: coffee.instaImg3 = data
                        default: break
                        }
                    }
                    
                    completion?(image)
                }
            }
        }
    }
    
}

//
//  FileLoadLocalManager.swift
//  DbTestSwift
//
//  Created by Рустам Мотыгуллин on 01.02.2021.
//

import Foundation

class LoadLocalManager {
    
    class func loadData(fileName: String) -> Data? {
        
        let mainBundle = Bundle.main
        guard let url = mainBundle.url(forResource: fileName, withExtension:"json") else { return nil }
        guard let data = try? Data(contentsOf: url) else { return nil }
        return data
    }
    
    class func loadCoffeemapMrusta(callback: @escaping ((CoffeeResponse) -> Void)) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            guard let data = loadData(fileName: "coffee-list") else { return }
            guard let fileResponse = try? CoffeeResponse(data: data) else { return }
            DispatchQueue.main.async {
                callback(fileResponse)
            }
        }
    }
    
    class func loadJson(fileName: String, callback: (( [String:Any] ) -> Void)? = nil ) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            guard let data = loadData(fileName: fileName) else { return }
            guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else { return }
            DispatchQueue.main.async {
                callback?(json)
            }
        }
    }
}

//
//  RequestDataManager.swift
//  DbTestSwift
//
//  Created by Рустам Мотыгуллин on 02.02.2021.
//

import Foundation

class RequestDataManager {
    
    class func request() {
        
        
        let srtURL = "http://rustamburger.phpnet.us/images/artist/15010316987670.jpg"
        guard let url = URL(string: srtURL) else { return }
        
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let currentData: Data = data, error == nil else { return }
            
            
            print(String(data: currentData, encoding: .utf8)!)
            
//            if let json = try? JSONSerialization.jsonObject(with: currentData, options: .allowFragments) as? [String : Any] {
//                
//            }
            
//            do {
//                if let json = try JSONSerialization.jsonObject(with: currentData, options: .allowFragments) as? [String : Any] {
//                    DispatchQueue.main.async {
//                        compJson?(currentData, json)
//                    }
//                }
//            } catch {
//                print(error)
//            }
        }).resume()
        
    }
    
}

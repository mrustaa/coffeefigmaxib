//
//  CoreDataManager.swift
//  DbTestSwift
//
//  Created by Рустам Мотыгуллин on 01.02.2021.
//

import Foundation
import UIKit
import CoreData

class CoreDataManager {
    
    class func getContext() -> NSManagedObjectContext {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        return context
    }
    
    class func save() {
        try? getContext().save()
    }
    
    class func getAddresses() -> [DbCoffeeAddress]? {
        guard let addresses = try? getContext().fetch(DbCoffeeAddress.fetchRequest()) // as? [DbCoffeeAddress]
      else { return nil }
        return addresses
    }
    
    class func getCoffees() -> [DbCoffee]? {
        guard let coffees = try? getContext().fetch(DbCoffee.fetchRequest()) //as? [DbCoffee]
      else { return nil }
        return coffees
    }
    
    class func getResponses() -> [DbCoffeeResponse]? {
        guard let responses = try? getContext().fetch(DbCoffeeResponse.fetchRequest()) //as? [DbCoffeeResponse]
      else { return nil }
        return responses
    }
    
    class func getCoffeeTitle(title: String) -> DbCoffee? {
        guard let coffees = getCoffees() else { return nil }
        for coffee in coffees {
            if coffee.title == title {
                return coffee
            }
        }
        return nil
    }
    
    class func saveDatabaseResponse(response: CoffeeResponse) {
        
        let context = getContext()
        
        let dbRes = DbCoffeeResponse(context: context)
        
        for new: CoffeeNew in response.new ?? [] {
            
            let dbNew = DbCoffeeNew(context: context)
            
            dbNew.response = dbRes
            dbNew.title = new.title
            dbNew.street = new.street
            
            dbRes.addToNew(dbNew)
        }
        
        for coffee: Coffee in response.response {
            
            let dbCoffee = DbCoffee(context: context)
            
            dbCoffee.response       = dbRes
            dbCoffee.title          = coffee.title
            dbCoffee.cappuccino     = Int64(coffee.cappuccino ?? 0)
            dbCoffee.content        = coffee.content
            dbCoffee.espresso       = Int64(coffee.espresso ?? 0)
            dbCoffee.imageURL       = coffee.imageURL
            dbCoffee.insta          = coffee.insta
            dbCoffee.stuff          = coffee.stuff
            dbCoffee.tags           = coffee.tags
            dbCoffee.title          = coffee.title
            dbCoffee.url            = coffee.url
            
            for address: CoffeeAddress in coffee.address {
                
                let dbCoffeeAddress = DbCoffeeAddress(context: context)
                
                dbCoffeeAddress.coffee = dbCoffee
                dbCoffeeAddress.street = address.street
                dbCoffeeAddress.time = address.time
                dbCoffeeAddress.lat = address.lat
                dbCoffeeAddress.long = address.long
                
                dbCoffee.addToAddress(dbCoffeeAddress)
            }
            
            dbRes.addToResponse(dbCoffee)
        }
        
        try? context.save()
    }
    
}

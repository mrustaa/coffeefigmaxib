//
//  InstagramDownloadManager.swift
//  DbTestSwift
//
//  Created by Рустам Мотыгуллин on 02.02.2021.
//

import WebKit
import UIKit
import Foundation

class InstagramDownloadManager: NSObject, WKNavigationDelegate {
    
    // MARK: - Properties
    
    static let current = InstagramDownloadManager()
    
    var webView: WKWebView?
    
    var webVC: OpenControllerURL?
    var vc: UIViewController?
    
    var saveHtmlCode: String?
    var saveJavaScript:  [String : Any]?
    var instaPhotosURLs: [String]?
    
    var htmlCodeCallback: ((String) -> ())?
    var photosURLsCallback: (([String]) -> ())?
    var errorPageCallback: (() -> Void)?
    
    // MARK: - Donwload Html Code
    
    func webViewDonwloadHtmlCode(strUrl: String,
                                 htmlCodeCallback: ((String) -> (Void))? = nil,
                                 errorPageCallback: (() -> Void)? = nil,
                                 photosURLsCallback: (([String]) -> Void)? = nil) {
        
        webView = WKWebView()
        webView?.navigationDelegate = self
        
        self.htmlCodeCallback = htmlCodeCallback
        self.errorPageCallback = errorPageCallback
        self.photosURLsCallback = photosURLsCallback
        
        guard let url = URL(string: strUrl) else { return }
            
//        if UIApplication.shared.canOpenURL(url) {
//            
//        } else {
//            
//        }
        
        let request = URLRequest(url: url)
        webView?.load(request)
        
        print(" donwload img 🚨 \(strUrl)")
    }
    
    // MARK: - WebView Open
    
    func webViewOpenUrl(strUrl: String, vc: UIViewController)  {
        
        self.vc = vc
        
        let webVc = OpenControllerURL()
        
        self.webVC = webVc
        
        vc.present(webVc, animated: true) {
            
        }
        
        webVc.navigateTo(strUrl: strUrl)
    }
    
    // MARK: - Check / Fix Instagram Url
    
    func checkInstagramUrl(coffee :DbCoffee) {
        
        /// проверяет инстаграмм ссылку
        if !coffee.instaUrlBad, coffee.instaImg1 == nil {
            if let insta = coffee.insta {
                
                if insta == "" {
                    coffee.instaUrlBad = true
                    CoreDataManager.save()
                } else {
                    
                    if let lastChar = insta.last {
                        if lastChar == "/" {
                            coffee.insta = String(insta.dropLast())
                            CoreDataManager.save()
                        }
                    }
                    
                    let instComp0 = insta.components(separatedBy: "https://instagram.com/")
                    if instComp0.count != 0 {
                        if instComp0[0] == "" {
                            coffee.insta = "https://www.instagram.com/\(instComp0[1])"
                            CoreDataManager.save()
                        }
                    }
                    
                    let instComp = insta.components(separatedBy: "instagram")
                    if instComp.count != 0 {
                        if instComp[0] == "" {
                            coffee.insta = "https://www.\(insta)"
                            CoreDataManager.save()
                        }
                    }
                    let instComp2 = insta.components(separatedBy: "www.instagram")
                    if instComp2.count != 0 {
                        if instComp2[0] == "" {
                            coffee.insta = "https://\(insta)"
                            CoreDataManager.save()
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Delegate
    
    
    
    // MARK: Get html-code
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        webView.evaluateJavaScript("document.documentElement.outerHTML.toString()",
                                   completionHandler: { [weak self] (html: Any?, error: Error?) in
                                    guard let _self = self else { return }
                                    guard let html = html as? String else { return }
                                    
                                    _self.saveHtmlCode = html
                                    
                                    _self.htmlCodeCallback?(html)
                                    
                                    _self.separationJavaScriptCode()
                                   })
    }
    
    // MARK: - Separation JS Code / Photos
    
    func separationJavaScriptCode() {
        
        let jsStr_ = separationHtmlCode(html: saveHtmlCode!,
                                       start: "<script type=\"text/javascript\">window._sharedData = ",
                                       end: ";</script>")
        
        if let jsStr = jsStr_ {
            guard let data = jsStr.data(using: .utf8) else { return }
            guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else { return }
            
            saveJavaScript = json
            
            var edges: [ [String : Any] ]?
            
            print(json)
            
            if let entry_data = json["entry_data"] as? [String : Any] {
                
                if let ProfilePage = entry_data["ProfilePage"] as? [ [String : Any] ] {
                    if ProfilePage.count != 0 {
                        if let graphql = ProfilePage[0]["graphql"] as? [String : Any] {
                            if let user = graphql["user"] as? [String : Any] {
                                if let edge_owner_to_timeline_media = user["edge_owner_to_timeline_media"] as? [String : Any] {
                                    if let edges_ = edge_owner_to_timeline_media["edges"] as? [ [String : Any] ] {
                                        edges = edges_
                                    }
                                }
                            }
                        }
                    }
                }
                
                if let _ = entry_data["HttpErrorPage"] {
                    errorPageCallback?()
                    return
                }
            }
            
            guard let edges_ = edges else { return }
            
            var arr: [String] = []
            
            for oneEdges in edges_ {
                
                if let node = oneEdges["node"] as? [String : Any] {
                    if let thumbnail_src = node["thumbnail_src"] as? String {
                        arr.append(thumbnail_src)
                    }
                }
            }
            
            instaPhotosURLs = arr
            
            photosURLsCallback?(arr)
            
        }
    }
    
    // MARK: Separation String start end
    
    func separationHtmlCode(html: String, start: String, end: String) -> String? {
        var arr: [String] = []
        let find = html.components(separatedBy: start)
        
        for i in 1..<find.count {
            let findStr = find[i]
            let find2 = findStr.components(separatedBy: end)
            if find.count != 0 {
                let find2Str = find2[0]
                arr.append(find2Str)
            }
        }
        
        if arr.count == 1 {
            return arr[0]
        }
        
        return nil
    }
    
}

// MARK: - WebView Controller

class OpenControllerURL: UIViewController,WKNavigationDelegate {
    
    var webView: WKWebView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let webView = WKWebView(frame: view.bounds)
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight] //It assigns Custom View height and width
        webView.navigationDelegate = self
        view.addSubview(webView)
        view.backgroundColor = UIColor.red
        self.webView = webView
    }
    
    func navigateTo(strUrl: String) {
        
        print(" open 🧩 \(strUrl)")
        
        guard let url = URL(string: strUrl) else { return }
        
        if UIApplication.shared.canOpenURL(url) {
            
        } else {
            
        }
        let request = URLRequest(url: url)
        webView?.load(request)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
    }
    
    
}

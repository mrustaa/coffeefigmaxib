//
//  CoffeeListContainer.swift
//  DbTestSwift
//
//  Created by Рустам Мотыгуллин on 02.02.2021.
//

import Foundation
import UIKit
import CoreData


class CoffeeListContainer: ContainerController {
    
    // MARK: - Properties
    
    var context: NSManagedObjectContext
    var items: [TableAdapterItem] = []
    var tableView: TableAdapterView?
    
    // MARK: - Init
    
    public init(addTo controller: UIViewController, context: NSManagedObjectContext) {
        
        let landLeft  = Device.screenMax - 350
        let landRight = Device.isIphoneXBottom + 20
        
        let layout = ContainerLayout()
        layout.startPosition = .top
        layout.backgroundShadowShow = true
        layout.insets            = ContainerInsets(right: 10, left: 10)
        layout.landscapeInsets   = ContainerInsets(right: landLeft, left: landRight)
        layout.positions         = ContainerPosition(top: 70, bottom: 120)
        layout.landscapePositions = ContainerPosition(top: 30, bottom: 120)
        
        self.context = context
        
        super.init(addTo: controller, layout: layout)
        
        loadContainerView()
        
        let tableView = createTableAdapterView(items: items, view: view)
        add(scrollView: tableView)
        self.tableView = tableView
        
        add(headerView: createHeaderView())
    }
    
    // MARK: - TableAdapterView
    
    func createTableAdapterView(items: [TableAdapterItem]? = nil, view: UIView) -> TableAdapterView {
        
        let table = TableAdapterView()
//        table.separatorColor = Colors.grayLevel(0.75)
        
        if let items = items {
            table.set(items: items, animated: true)
        }
        
        table.didScrollCallback = {
            view.endEditing(true)
        }
        return table
    }
    
    //MARK: - Load Container-View
    
    func loadContainerView() {
        
        view.backgroundColor = .white
        view.cornerRadius = 15
        view.addShadow()
    }
    
    //MARK: - Load Header-View
    
    func createHeaderView() -> ExampleHeaderGripView {
        
        let headerView = ExampleHeaderGripView()
        headerView.height = 20
        return headerView
    }
    
    //MARK: - Load Items
    
    func loadTableItems() {
        
    }
    
    //MARK: - Update Table
    
    func updateTable() {
        
        items = []
        
        guard let addresses = CoreDataManager.getAddresses() else { return }
        
        for address: DbCoffeeAddress in addresses {
            items.append( CoffeeListItem(dbAddress: address) )
        }
        
        
        //        for _ in 1...23 {
        //
        //            items.append( CoffeeListItem(title: "Injir", distance: "664 м", time: " · Работает до 22:00", price: " · 100-180 ₽" ) )
        //            items.append( CoffeeListItem(title: "Crop.", distance: "664 м", time: " · Работает до 22:00", price: " · 100-150 ₽" ) )
        //            items.append( CoffeeListItem(title: "1554",  distance: "664 м", time: " · Откроется в 8:00",  price: " · 100-200 ₽" ) )
        //
        //            items.append( CoffeeListItem(title: "Injir", distance: "664 м", time: " · Работает до 22:00", price: " · 100-180 ₽" ) )
        //            items.append( CoffeeListItem(title: "Injir", distance: "664 м", time: " · Работает до 22:00", price: " · 100-180 ₽" ) )
        //
        //        }
        
        self.tableView?.set(items: items, animated: true)
        
        //tableView.set(items: items, animated: true)
    }
    
}

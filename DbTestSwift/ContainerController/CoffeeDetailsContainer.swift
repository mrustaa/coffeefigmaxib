//
//  CoffeeListContainer.swift
//  DbTestSwift
//
//  Created by Рустам Мотыгуллин on 02.02.2021.
//

import Foundation
import UIKit
import CoreData


class CoffeeDetailsContainer: ContainerController {
    
    // MARK: - Properties
    
    var context: NSManagedObjectContext
    var items: [TableAdapterItem] = []
    var tableView: TableAdapterView?
    
    var address : DbCoffeeAddress?
    var coffee : DbCoffee?
    
    var closeClickCallback: (() -> ())?
    
    // MARK: - Init
    
    public init(addTo controller: UIViewController, context: NSManagedObjectContext) {
        
        let landLeft  = Device.screenMax - 350
        let landRight = Device.isIphoneXBottom + 20
        
        
        let layout = ContainerLayout()
        layout.startPosition = .hide
        layout.backgroundShadowShow = true
        layout.landscapeBackgroundShadowShow = false
        layout.insets            = ContainerInsets(right: 20, left: 20)
        layout.landscapeInsets   = ContainerInsets(right: landRight, left: landLeft)
        layout.positions          = ContainerPosition(top: 70, bottom: 170)
        layout.landscapePositions = ContainerPosition(top: 20, bottom: 140)
        
        self.context = context
        
        super.init(addTo: controller, layout: layout)
        
        loadContainerView()
        
        let tableView = createTableAdapterView(items: items, view: view)
        add(scrollView: tableView)
        self.tableView = tableView
        
        loadTableItems()
        
        //add(headerView: createHeaderView())
    }
    
    // MARK: - TableAdapterView
    
    func createTableAdapterView(items: [TableAdapterItem]? = nil, view: UIView) -> TableAdapterView {
        
        let table = TableAdapterView()
//        table.separatorColor = Colors.grayLevel(0.75)
        
        if let items = items {
            table.set(items: items, animated: true)
        }
        
        table.didScrollCallback = {
            view.endEditing(true)
        }
        return table
    }
    
    //MARK: - Load Container-View
    
    func loadContainerView() {
        
        view.backgroundColor = .white
        view.cornerRadius = 15
        view.addShadow()
    }
    
    //MARK: - Load Header-View
    
    func createHeaderView() -> ExampleHeaderGripView {
        
        let headerView = ExampleHeaderGripView()
        headerView.height = 20
        return headerView
    }
    
    //MARK: - Load Items
    
    func loadTableItems() {
        items = []
        self.tableView?.set(items: items, animated: true)
    }
    
    //MARK: - Update Table
    
    func updateTable(indexCoreData: Int = 0) {
        
        items = []
        
        guard let addresses = CoreDataManager.getAddresses() else { return }
        
        let address :DbCoffeeAddress = addresses[indexCoreData]
        guard let coffee :DbCoffee = address.coffee else { return }
        
        self.address = address
        self.coffee = coffee
        
        
        InstagramDownloadManager.current.checkInstagramUrl(coffee: coffee)
        
        
        items.append( CoffeeDetailsNewNameItem(text1: coffee.title, closeClickCallback: { [weak self] in
            guard let _self = self else { return }
            _self.closeClickCallback?()
        }) )
        items.append( CoffeeDetailsNewRouteItem() )
        items.append( CoffeeDetailsNewAboutItem(text0: coffee.content) )
        
        if !coffee.instaUrlBad {
            items.append( CoffeeDetailsNewInstaItem(dbCoffee: coffee) )
            items.append( CoffeeDetailsNewLinkItem() )
        }
        
        items.append( CoffeeDetailsNewSectionItem(text0: "Кофе") )
        
        items.append( CoffeeDetailsNewInfoItem(title: "Цены",
                                               text1: "Эспрессо", text2: "Капучино", text3: "\(coffee.espresso) ₽", text4: "\(coffee.cappuccino) ₽") )
        
        items.append( CoffeeDetailsNewInfoMiniItem(text0: "Обжарщик",
                                                   text1: coffee.stuff) )
        
        
        
        if coffee.tags != nil {
            var tagsStr = ""
            if let tags = coffee.tags {
                tagsStr = tags.map { "\(String($0)), " }.joined()
            }
            
            items.append( CoffeeDetailsNewInfoMiniItem(text0: "Типы кофе", text1: tagsStr ))
        }
        
        
        items.append( CoffeeDetailsNewSeparatorItem() )
        
        items.append( CoffeeDetailsNewSectionItem(text0: "Кофейня") )
        items.append( CoffeeDetailsNewInfoMiniItem(text0: "Часы работы", text1: address.time, text2: "") )
        items.append( CoffeeDetailsNewInfoMiniItem(text0: "Адрес", text1: address.street) )
        
        items.append( CoffeeDetailsNewSeparatorItem() )
        
        tableView?.set(items: items, animated: true)
    }
    
}

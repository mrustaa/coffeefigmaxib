//
//  ViewController.swift
//  DbTestSwift
//
//  Created by Рустам Мотыгуллин on 31.01.2021.
//

import UIKit
import CoreData
import MapKit


class ViewController: UIViewController {

    // MARK: - Properties
    
  @IBOutlet var mapView: MapViewWithZoom!
  var tableView: TableAdapterView!
    
    var listContainer: CoffeeListContainer?
    var detailsContainer: CoffeeDetailsContainer?
    
    let context: NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    let mskCoordinate = CLLocationCoordinate2DMake(55.751244, 37.618423)
    self.mapView.setCenterCoordinate(coordinate: mskCoordinate, zoomLevel: 10, animated: true)
    
  }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        listContainer = CoffeeListContainer(addTo: self, context: context)
        listContainer?.tableView?.selectIndexCallback = { [weak self] (index) in
            guard let _self = self else { return }
            if Device.isPortrait {
                _self.listContainer?.move(type: .hide)
            }
            _self.detailsContainer?.updateTable(indexCoreData: index)
            _self.detailsContainer?.move(type: .top)
        }
        
        detailsContainer = CoffeeDetailsContainer(addTo: self, context: context)
        detailsContainer?.closeClickCallback = { [weak self] in
            guard let _self = self else { return }
            _self.detailsContainer?.move(type: .hide)
            _self.listContainer?.move(type: .top)
        }
        detailsContainer?.tableView?.selectIndexCallback = { [weak self] (index) in
            guard let _self = self else { return }
            if index == 4 {
                _self.loginInstagram()
            }
        }
        
        guard let responses = CoreDataManager.getResponses() else { return }
        
        if responses.count == 0 {
            
            LoadLocalManager.loadCoffeemapMrusta { [weak self] (response: CoffeeResponse) in
                CoreDataManager.saveDatabaseResponse(response: response)
                
                self?.listContainer?.updateTable()
            }
            
        } else {
            listContainer?.updateTable()
        }
        
    }
    
    func loginInstagram() {
        guard let instaUrl = detailsContainer?.coffee?.insta else { return }
        InstagramDownloadManager.current.webViewOpenUrl(strUrl: instaUrl, vc: self)
    }
    
//
    
}


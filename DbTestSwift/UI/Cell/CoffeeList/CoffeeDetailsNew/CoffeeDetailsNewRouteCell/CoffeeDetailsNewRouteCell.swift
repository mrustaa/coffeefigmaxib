import UIKit

// MARK: - Item

class CoffeeDetailsNewRouteItem: TableAdapterItem {
    
    init(image0: UIImage? = nil,
         image1: UIImage? = nil,
         text2: String? = nil,
         separator: Bool = false,
         touchAnimationHide: Bool = false,
         editing: Bool = false,
         clickCallback: (() -> ())? = nil) {
        
        let cellData = CoffeeDetailsNewRouteCellData(image0: image0,
                                    image1: image1,
                                    text2: text2,
                                    separator: separator,
                                        touchAnimationHide: touchAnimationHide,
                                        editing: editing,
                                        clickCallback: clickCallback)
        
        super.init(cellClass: CoffeeDetailsNewRouteCell.self, cellData: cellData)
    }
}

// MARK: - Data

class CoffeeDetailsNewRouteCellData: TableAdapterCellData {
    
    // MARK: Properties
    
    var image0: UIImage?
    var image1: UIImage?
    var text2: String?
    
    var separatorVisible: Bool
    var touchAnimationHide: Bool
    var editing: Bool
    
    // MARK: Inits
    
    init(image0: UIImage? = nil,
         image1: UIImage? = nil,
         text2: String? = nil,
         
         separator: Bool,
         touchAnimationHide: Bool,
         editing: Bool,
         clickCallback: (() -> ())?) {
        
        self.image0 = image0
        self.image1 = image1
        self.text2 = text2
        
        self.separatorVisible = separator
        self.touchAnimationHide = touchAnimationHide
        self.editing = editing
        
        super.init(clickCallback)
    }
    
    override public func cellHeight() -> CGFloat {
        return 52.0
    }
    
    override public func canEditing() -> Bool {
        return editing
    }
}

// MARK: - Cell

class CoffeeDetailsNewRouteCell: TableAdapterCell {
    
    // MARK: Properties
    
    public var data: CoffeeDetailsNewRouteCellData?
    
    // MARK: Outlets
    
    @IBOutlet private weak var imageView0: UIImageView?
    @IBOutlet private weak var imageView1: UIImageView?
    @IBOutlet private weak var label2: UILabel?
    
    @IBOutlet override var selectedView: UIView? { didSet { } }
    
    // MARK: Initialize
    
    override func awakeFromNib() {
        separator(hide: true)
    }
    
    override func fill(data: TableAdapterCellData?) {
        guard let data = data as? CoffeeDetailsNewRouteCellData else { return }
        self.data = data
        
        self.hideAnimation = data.touchAnimationHide
        separator(hide: !data.separatorVisible)
        
        imageView0?.image = data.image0 ?? imageView0?.image
        imageView1?.image = data.image1 ?? imageView1?.image
        label2?.text = data.text2 ?? label2?.text
        
    }
}
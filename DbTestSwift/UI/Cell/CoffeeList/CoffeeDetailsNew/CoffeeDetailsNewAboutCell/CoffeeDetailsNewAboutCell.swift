import UIKit

// MARK: - Item

class CoffeeDetailsNewAboutItem: TableAdapterItem {
    
    init(text0: String? = nil,
         separator: Bool = false,
         touchAnimationHide: Bool = false,
         editing: Bool = false,
         clickCallback: (() -> ())? = nil) {
        
        let cellData = CoffeeDetailsNewAboutCellData(text0: text0,
                                    separator: separator,
                                        touchAnimationHide: touchAnimationHide,
                                        editing: editing,
                                        clickCallback: clickCallback)
        
        super.init(cellClass: CoffeeDetailsNewAboutCell.self, cellData: cellData)
    }
}

// MARK: - Data

class CoffeeDetailsNewAboutCellData: TableAdapterCellData {
    
    // MARK: Properties
    
    var text0: String?
    
    var separatorVisible: Bool
    var touchAnimationHide: Bool
    var editing: Bool
    
    // MARK: Inits
    
    init(text0: String? = nil,
         
         separator: Bool,
         touchAnimationHide: Bool,
         editing: Bool,
         clickCallback: (() -> ())?) {
        
        self.text0 = text0
        
        self.separatorVisible = separator
        self.touchAnimationHide = touchAnimationHide
        self.editing = editing
        
        super.init(clickCallback)
    }
    
    override public func cellHeight() -> CGFloat {
        return 100.0
    }
    
    override public func canEditing() -> Bool {
        return editing
    }
}

// MARK: - Cell

class CoffeeDetailsNewAboutCell: TableAdapterCell {
    
    // MARK: Properties
    
    public var data: CoffeeDetailsNewAboutCellData?
    
    // MARK: Outlets
    
    @IBOutlet private weak var label0: UILabel?
    
    @IBOutlet override var selectedView: UIView? { didSet { } }
    
    // MARK: Initialize
    
    override func awakeFromNib() {
        separator(hide: true)
    }
    
    override func fill(data: TableAdapterCellData?) {
        guard let data = data as? CoffeeDetailsNewAboutCellData else { return }
        self.data = data
        
        self.hideAnimation = data.touchAnimationHide
        separator(hide: !data.separatorVisible)
        
        label0?.text = data.text0 ?? label0?.text
        
    }
}
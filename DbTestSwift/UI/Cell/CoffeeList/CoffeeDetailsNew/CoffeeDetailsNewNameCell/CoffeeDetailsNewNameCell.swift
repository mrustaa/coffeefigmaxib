import UIKit

// MARK: - Item

class CoffeeDetailsNewNameItem: TableAdapterItem {
    
    init(text0: String? = nil,
         text1: String? = nil,
         image2: UIImage? = nil,
         separator: Bool = false,
         touchAnimationHide: Bool = false,
         editing: Bool = false,
         closeClickCallback: (() -> ())? = nil,
         clickCallback: (() -> ())? = nil) {
        
        let cellData = CoffeeDetailsNewNameCellData(text0: text0,
                                    text1: text1,
                                    image2: image2,
                                    separator: separator,
                                        touchAnimationHide: touchAnimationHide,
                                        editing: editing,
                                        closeClickCallback: closeClickCallback,
                                        clickCallback: clickCallback)
        
        super.init(cellClass: CoffeeDetailsNewNameCell.self, cellData: cellData)
    }
}

// MARK: - Data

class CoffeeDetailsNewNameCellData: TableAdapterCellData {
    
    // MARK: Properties
    
    var text0: String?
    var text1: String?
    var image2: UIImage?
    
    var separatorVisible: Bool
    var touchAnimationHide: Bool
    var editing: Bool
    
    var closeClickCallback: (() -> ())?
    
    // MARK: Inits
    
    init(text0: String? = nil,
         text1: String? = nil,
         image2: UIImage? = nil,
         
         separator: Bool,
         touchAnimationHide: Bool,
         editing: Bool,
         closeClickCallback: (() -> ())? = nil,
         clickCallback: (() -> ())?) {
        
        self.text0 = text0
        self.text1 = text1
        self.image2 = image2
        
        self.closeClickCallback = closeClickCallback
        
        self.separatorVisible = separator
        self.touchAnimationHide = touchAnimationHide
        self.editing = editing
        
        super.init(clickCallback)
    }
    
    override public func cellHeight() -> CGFloat {
        return 90.0
    }
    
    override public func canEditing() -> Bool {
        return editing
    }
}

// MARK: - Cell

class CoffeeDetailsNewNameCell: TableAdapterCell {
    
    // MARK: Properties
    
    public var data: CoffeeDetailsNewNameCellData?
    
    // MARK: Outlets
    
    @IBOutlet private weak var label0: UILabel?
    @IBOutlet private weak var label1: UILabel?
    @IBOutlet private weak var imageView2: UIImageView?
    
    @IBOutlet override var selectedView: UIView? { didSet { } }
    
    // MARK: Initialize
    
    override func awakeFromNib() {
        separator(hide: true)
    }
    
    override func fill(data: TableAdapterCellData?) {
        guard let data = data as? CoffeeDetailsNewNameCellData else { return }
        self.data = data
        
        self.hideAnimation = data.touchAnimationHide
        separator(hide: !data.separatorVisible)
        
        label0?.text = data.text0 ?? label0?.text
        label1?.text = data.text1 ?? label1?.text
        imageView2?.image = data.image2 ?? imageView2?.image
        
    }
    
    @IBAction func buttonCloseAction(_ sender: Any) {
        data?.closeClickCallback?()
    }
}

import UIKit

// MARK: - Item

class CoffeeDetailsNewInfoItem: TableAdapterItem {
    
    init(title: String? = nil,
         text1: String? = nil,
         text2: String? = nil,
         text3: String? = nil,
         text4: String? = nil,
         separator: Bool = false,
         touchAnimationHide: Bool = false,
         editing: Bool = false,
         clickCallback: (() -> ())? = nil) {
        
        let cellData = CoffeeDetailsNewInfoCellData(title: title,
                                    text1: text1,
                                    text2: text2,
                                    text3: text3,
                                    text4: text4,
                                    separator: separator,
                                        touchAnimationHide: touchAnimationHide,
                                        editing: editing,
                                        clickCallback: clickCallback)
        
        super.init(cellClass: CoffeeDetailsNewInfoCell.self, cellData: cellData)
    }
}

// MARK: - Data

class CoffeeDetailsNewInfoCellData: TableAdapterCellData {
    
    // MARK: Properties
    
    var title: String?
    var text1: String?
    var text2: String?
    var text3: String?
    var text4: String?
    
    var separatorVisible: Bool
    var touchAnimationHide: Bool
    var editing: Bool
    
    // MARK: Inits
    
    init(title: String? = nil,
         text1: String? = nil,
         text2: String? = nil,
         text3: String? = nil,
         text4: String? = nil,
         
         separator: Bool,
         touchAnimationHide: Bool,
         editing: Bool,
         clickCallback: (() -> ())?) {
        
        self.title = title
        self.text1 = text1
        self.text2 = text2
        self.text3 = text3
        self.text4 = text4
        
        self.separatorVisible = separator
        self.touchAnimationHide = touchAnimationHide
        self.editing = editing
        
        super.init(clickCallback)
    }
    
    override public func cellHeight() -> CGFloat {
        return 90.0
    }
    
    override public func canEditing() -> Bool {
        return editing
    }
}

// MARK: - Cell

class CoffeeDetailsNewInfoCell: TableAdapterCell {
    
    // MARK: Properties
    
    public var data: CoffeeDetailsNewInfoCellData?
    
    // MARK: Outlets
    
    @IBOutlet private weak var label0: UILabel?
    
    @IBOutlet private weak var label1: UILabel?
    @IBOutlet private weak var label2: UILabel?
    
    @IBOutlet private weak var label3: UILabel?
    @IBOutlet private weak var label4: UILabel?
    
    @IBOutlet override var selectedView: UIView? { didSet { } }
    
    // MARK: Initialize
    
    override func awakeFromNib() {
        separator(hide: true)
    }
    
    override func fill(data: TableAdapterCellData?) {
        guard let data = data as? CoffeeDetailsNewInfoCellData else { return }
        self.data = data
        
        self.hideAnimation = data.touchAnimationHide
        separator(hide: !data.separatorVisible)
        
        label0?.text = data.title ?? label0?.text
        label1?.text = data.text1 ?? label1?.text
        label2?.text = data.text2 ?? label2?.text
        label3?.text = data.text3 ?? label3?.text
        label4?.text = data.text4 ?? label4?.text
        
    }
}

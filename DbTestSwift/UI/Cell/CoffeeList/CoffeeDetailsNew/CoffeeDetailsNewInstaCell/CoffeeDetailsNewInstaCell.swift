import UIKit

// MARK: - Item

class CoffeeDetailsNewInstaItem: TableAdapterItem {
    
    init(dbCoffee: DbCoffee,
         clickCallback: (() -> ())? = nil) {
        
        let cellData = CoffeeDetailsNewInstaCellData(dbCoffee: dbCoffee, clickCallback: clickCallback)
        super.init(cellClass: CoffeeDetailsNewInstaCell.self, cellData: cellData)
    }
    
    init(image0: UIImage? = nil,
         image1: UIImage? = nil,
         image2: UIImage? = nil,
         separator: Bool = false,
         touchAnimationHide: Bool = false,
         editing: Bool = false,
         clickCallback: (() -> ())? = nil) {
        
        let cellData = CoffeeDetailsNewInstaCellData(image0: image0,
                                    image1: image1,
                                    image2: image2,
                                    separator: separator,
                                        touchAnimationHide: touchAnimationHide,
                                        editing: editing,
                                        clickCallback: clickCallback)
        
        super.init(cellClass: CoffeeDetailsNewInstaCell.self, cellData: cellData)
    }
}

// MARK: - Data

class CoffeeDetailsNewInstaCellData: TableAdapterCellData {
    
    // MARK: Properties
    
    var image0: UIImage?
    var image1: UIImage?
    var image2: UIImage?
    
    var separatorVisible: Bool = false
    var touchAnimationHide: Bool = false
    var editing: Bool = false
    
    var dbCoffee: DbCoffee?
    
    // MARK: Inits
    
    init(dbCoffee: DbCoffee, clickCallback: (() -> ())?) {
        
        self.dbCoffee = dbCoffee
        
        super.init(clickCallback)
    }
    
    
    init(image0: UIImage? = nil,
         image1: UIImage? = nil,
         image2: UIImage? = nil,
         
         separator: Bool,
         touchAnimationHide: Bool,
         editing: Bool,
         clickCallback: (() -> ())?) {
        
        self.image0 = image0
        self.image1 = image1
        self.image2 = image2
        
        self.separatorVisible = separator
        self.touchAnimationHide = touchAnimationHide
        self.editing = editing
        
        super.init(clickCallback)
    }
    
    override public func cellHeight() -> CGFloat {
        return 200.0
    }
    
    override public func canEditing() -> Bool {
        return editing
    }
}

// MARK: - Cell

class CoffeeDetailsNewInstaCell: TableAdapterCell {
    
    // MARK: Properties
    
    public var data: CoffeeDetailsNewInstaCellData?
    
    // MARK: Outlets
    
    @IBOutlet private weak var imageView0: UIImageView?
    @IBOutlet private weak var imageView1: UIImageView?
    @IBOutlet private weak var imageView2: UIImageView?
    
    @IBOutlet override var selectedView: UIView? { didSet { } }
    
    // MARK: Initialize
    
    override func awakeFromNib() {
        separator(hide: true)
    }
    
    override func fill(data: TableAdapterCellData?) {
        guard let data = data as? CoffeeDetailsNewInstaCellData else { return }
        self.data = data
        
        self.hideAnimation = data.touchAnimationHide
        separator(hide: !data.separatorVisible)
        
        if data.image0 != nil || data.image1 != nil || data.image2 != nil {
            self.imageView0?.image = data.image0
            self.imageView1?.image = data.image1
            self.imageView2?.image = data.image2
            return
        }
        
        
        if let imgData1 = data.dbCoffee?.instaImg1,
           let imgData2 = data.dbCoffee?.instaImg2,
           let imgData3 = data.dbCoffee?.instaImg3 {
            
            imageView0?.alpha = 1
            imageView1?.alpha = 1
            imageView2?.alpha = 1
            
            if let image = UIImage(data: imgData1) {
                data.image0 = image
                imageView0?.image = image
            }
            if let image = UIImage(data: imgData2) {
                data.image1 = image
                imageView1?.image = image
            }
            if let image = UIImage(data: imgData3) {
                data.image2 = image
                imageView2?.image = image
            }
            
        } else {
            
            imageView0?.alpha = 0
            imageView1?.alpha = 0
            imageView2?.alpha = 0
            
            instagramDonwloadImages()
        }
        
        
    }
    
    func instagramDonwloadImages() {
        
        guard let insta = data?.dbCoffee?.insta else { return }
        
        InstagramDownloadManager.current.webViewDonwloadHtmlCode(strUrl: insta, errorPageCallback: { [weak self] in
            guard let _self = self else { return }
            
            _self.data?.dbCoffee?.instaUrlBad = true
            CoreDataManager.save()
            
        }, photosURLsCallback: { [weak self] (photosURLs) in
            guard let _self = self else { return }
            
            if photosURLs.count == 1 {
                _self.downloadImageURL(urlStr: photosURLs[0], index: 0)
            } else if photosURLs.count == 2 {
                _self.downloadImageURL(urlStr: photosURLs[0], index: 0)
                _self.downloadImageURL(urlStr: photosURLs[1], index: 1)
            } else {
                _self.downloadImageURL(urlStr: photosURLs[0], index: 0)
                _self.downloadImageURL(urlStr: photosURLs[1], index: 1)
                _self.downloadImageURL(urlStr: photosURLs[2], index: 2)
            }
        })
    }
    
    func downloadImageURL(urlStr: String, index: Int) {
        
        guard let title = data?.dbCoffee?.title else { return }
        
        ImageDownload.downloadBdImage(urlStr: urlStr, coffeeTitle: title, index: index) { [weak self] (image) in
            guard let _self = self else { return }
            
            switch index {
            case 0:
                _self.data?.image0 = image
                _self.setAnimationImageView(imageView: _self.imageView0, image: image)
            case 1:
                _self.data?.image1 = image
                _self.setAnimationImageView(imageView: _self.imageView1, image: image)
            case 2:
                _self.data?.image2 = image
                _self.setAnimationImageView(imageView: _self.imageView2, image: image)
            default: break
            }
            
            CoreDataManager.save()
        }
    }
    
    func setAnimationImageView(imageView: UIImageView?, image: UIImage) {
        imageView?.image = image
        UIView.animate(withDuration: 0.5) {
            imageView?.alpha = 1
        }
    }
}

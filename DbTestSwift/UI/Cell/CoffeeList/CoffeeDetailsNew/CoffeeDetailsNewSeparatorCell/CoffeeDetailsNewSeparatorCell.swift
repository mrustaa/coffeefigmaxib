import UIKit

// MARK: - Item

class CoffeeDetailsNewSeparatorItem: TableAdapterItem {
    
    init(separator: Bool = false,
         touchAnimationHide: Bool = false,
         editing: Bool = false,
         clickCallback: (() -> ())? = nil) {
        
        let cellData = CoffeeDetailsNewSeparatorCellData(separator: separator,
                                        touchAnimationHide: touchAnimationHide,
                                        editing: editing,
                                        clickCallback: clickCallback)
        
        super.init(cellClass: CoffeeDetailsNewSeparatorCell.self, cellData: cellData)
    }
}

// MARK: - Data

class CoffeeDetailsNewSeparatorCellData: TableAdapterCellData {
    
    // MARK: Properties
    
    
    var separatorVisible: Bool
    var touchAnimationHide: Bool
    var editing: Bool
    
    // MARK: Inits
    
    init(
         separator: Bool,
         touchAnimationHide: Bool,
         editing: Bool,
         clickCallback: (() -> ())?) {
        
        
        self.separatorVisible = separator
        self.touchAnimationHide = touchAnimationHide
        self.editing = editing
        
        super.init(clickCallback)
    }
    
    override public func cellHeight() -> CGFloat {
        return 16.0
    }
    
    override public func canEditing() -> Bool {
        return editing
    }
}

// MARK: - Cell

class CoffeeDetailsNewSeparatorCell: TableAdapterCell {
    
    // MARK: Properties
    
    public var data: CoffeeDetailsNewSeparatorCellData?
    
    // MARK: Outlets
    
    
    @IBOutlet override var selectedView: UIView? { didSet { } }
    
    // MARK: Initialize
    
    override func awakeFromNib() {
        separator(hide: true)
    }
    
    override func fill(data: TableAdapterCellData?) {
        guard let data = data as? CoffeeDetailsNewSeparatorCellData else { return }
        self.data = data
        
        self.hideAnimation = data.touchAnimationHide
        separator(hide: !data.separatorVisible)
        
        
    }
}
import UIKit

// MARK: - Item

class CoffeeDetailsNewInfoMiniItem: TableAdapterItem {
    
    init(text0: String? = nil,
         text1: String? = nil,
         text2: String? = nil,
         separator: Bool = false,
         touchAnimationHide: Bool = false,
         editing: Bool = false,
         clickCallback: (() -> ())? = nil) {
        
        let cellData = CoffeeDetailsNewInfoMiniCellData(text0: text0,
                                    text1: text1,
                                    text2: text2,
                                    separator: separator,
                                        touchAnimationHide: touchAnimationHide,
                                        editing: editing,
                                        clickCallback: clickCallback)
        
        super.init(cellClass: CoffeeDetailsNewInfoMiniCell.self, cellData: cellData)
    }
}

// MARK: - Data

class CoffeeDetailsNewInfoMiniCellData: TableAdapterCellData {
    
    // MARK: Properties
    
    var text0: String?
    var text1: String?
    var text2: String?
    
    var separatorVisible: Bool
    var touchAnimationHide: Bool
    var editing: Bool
    
    // MARK: Inits
    
    init(text0: String? = nil,
         text1: String? = nil,
         text2: String? = nil,
         
         separator: Bool,
         touchAnimationHide: Bool,
         editing: Bool,
         clickCallback: (() -> ())?) {
        
        self.text0 = text0
        self.text1 = text1
        self.text2 = text2
        
        self.separatorVisible = separator
        self.touchAnimationHide = touchAnimationHide
        self.editing = editing
        
        super.init(clickCallback)
    }
    
    override public func cellHeight() -> CGFloat {
        return 68.0
    }
    
    override public func canEditing() -> Bool {
        return editing
    }
}

// MARK: - Cell

class CoffeeDetailsNewInfoMiniCell: TableAdapterCell {
    
    // MARK: Properties
    
    public var data: CoffeeDetailsNewInfoMiniCellData?
    
    // MARK: Outlets
    
    @IBOutlet weak var button: DesignButton!
    @IBOutlet private weak var label0: UILabel?
    @IBOutlet private weak var label1: UILabel?
    @IBOutlet private weak var label2: UILabel?
    
    @IBOutlet override var selectedView: UIView? { didSet { } }
    
    // MARK: Initialize
    
    override func awakeFromNib() {
        separator(hide: true)
    }
    
    override func fill(data: TableAdapterCellData?) {
        guard let data = data as? CoffeeDetailsNewInfoMiniCellData else { return }
        self.data = data
        
        self.hideAnimation = data.touchAnimationHide
        separator(hide: !data.separatorVisible)
        
        label0?.text = data.text0 ?? label0?.text
        label1?.text = data.text1 ?? label1?.text
        
        if (data.text2 != nil) {
            button.alpha = 1.0
        } else {
            button.alpha = 0.0
        }
        
    }
}

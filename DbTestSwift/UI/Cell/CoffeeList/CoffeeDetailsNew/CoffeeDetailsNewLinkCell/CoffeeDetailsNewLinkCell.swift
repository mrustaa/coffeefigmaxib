import UIKit

// MARK: - Item

class CoffeeDetailsNewLinkItem: TableAdapterItem {
    
    init(image0: UIImage? = nil,
         text1: String? = nil,
         separator: Bool = false,
         touchAnimationHide: Bool = false,
         editing: Bool = false,
         clickCallback: (() -> ())? = nil) {
        
        let cellData = CoffeeDetailsNewLinkCellData(image0: image0,
                                    text1: text1,
                                    separator: separator,
                                        touchAnimationHide: touchAnimationHide,
                                        editing: editing,
                                        clickCallback: clickCallback)
        
        super.init(cellClass: CoffeeDetailsNewLinkCell.self, cellData: cellData)
    }
}

// MARK: - Data

class CoffeeDetailsNewLinkCellData: TableAdapterCellData {
    
    // MARK: Properties
    
    var image0: UIImage?
    var text1: String?
    
    var separatorVisible: Bool
    var touchAnimationHide: Bool
    var editing: Bool
    
    // MARK: Inits
    
    init(image0: UIImage? = nil,
         text1: String? = nil,
         
         separator: Bool,
         touchAnimationHide: Bool,
         editing: Bool,
         clickCallback: (() -> ())?) {
        
        self.image0 = image0
        self.text1 = text1
        
        self.separatorVisible = separator
        self.touchAnimationHide = touchAnimationHide
        self.editing = editing
        
        super.init(clickCallback)
    }
    
    override public func cellHeight() -> CGFloat {
        return 64.0
    }
    
    override public func canEditing() -> Bool {
        return editing
    }
}

// MARK: - Cell

class CoffeeDetailsNewLinkCell: TableAdapterCell {
    
    // MARK: Properties
    
    public var data: CoffeeDetailsNewLinkCellData?
    
    // MARK: Outlets
    
    @IBOutlet private weak var imageView0: UIImageView?
    @IBOutlet private weak var label1: UILabel?
    
    @IBOutlet override var selectedView: UIView? { didSet { } }
    
    // MARK: Initialize
    
    override func awakeFromNib() {
        separator(hide: true)
    }
    
    override func fill(data: TableAdapterCellData?) {
        guard let data = data as? CoffeeDetailsNewLinkCellData else { return }
        self.data = data
        
        self.hideAnimation = data.touchAnimationHide
        separator(hide: !data.separatorVisible)
        
        imageView0?.image = data.image0 ?? imageView0?.image
        label1?.text = data.text1 ?? label1?.text
        
    }
}
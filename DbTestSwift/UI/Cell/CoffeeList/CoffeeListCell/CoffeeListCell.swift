import UIKit

// MARK: - Item

class CoffeeListItem: TableAdapterItem {
    
    init(dbAddress: DbCoffeeAddress,
         clickCallback: (() -> ())? = nil) {
        
        let cellData = CoffeeListCellData(dbAddress: dbAddress, clickCallback: clickCallback)
        super.init(cellClass: CoffeeListCell.self, cellData: cellData)
    }
    
    init(title: String? = nil,
         distance: String? = nil,
         time: String? = nil,
         price: String? = nil,
         separator: Bool = false,
         touchAnimationHide: Bool = false,
         editing: Bool = false,
         clickCallback: (() -> ())? = nil) {
        
        let cellData = CoffeeListCellData(text0: time,
                                    text1: price,
                                    text2: distance,
                                    text3: title, // title
                                    separator: separator,
                                        touchAnimationHide: touchAnimationHide,
                                        editing: editing,
                                        clickCallback: clickCallback)
        
        super.init(cellClass: CoffeeListCell.self, cellData: cellData)
    }
}

// MARK: - Data

class CoffeeListCellData: TableAdapterCellData {
    
    // MARK: Properties
    
    var text0: String?
    var text1: String?
    var text2: String?
    var text3: String?
    
    var separatorVisible: Bool
    var touchAnimationHide: Bool
    var editing: Bool
    
    var dbAddress: DbCoffeeAddress?
    
    // MARK: Inits
    
    init(dbAddress: DbCoffeeAddress, clickCallback: (() -> ())?) {
        
        self.dbAddress = dbAddress
        
        self.text0 = " · Работает до 23:00" /// time
        self.text1 = " · " + "\(dbAddress.coffee?.espresso ?? 0)" + "-" + "\(dbAddress.coffee?.cappuccino ?? 0)" + " ₽" /// price
        self.text2 = "963 м" ///distance
        self.text3 = dbAddress.coffee?.title
        
        self.separatorVisible = false
        self.touchAnimationHide = false
        self.editing = false
        
        super.init(clickCallback)
    }
    
    init(text0: String? = nil,
         text1: String? = nil,
         text2: String? = nil,
         text3: String? = nil,
         
         separator: Bool,
         touchAnimationHide: Bool,
         editing: Bool,
         clickCallback: (() -> ())?) {
        
        self.text0 = text0
        self.text1 = text1
        self.text2 = text2
        self.text3 = text3
        
        self.separatorVisible = separator
        self.touchAnimationHide = touchAnimationHide
        self.editing = editing
        
        super.init(clickCallback)
    }
    
    override public func cellHeight() -> CGFloat {
        return 88.0
    }
    
    override public func canEditing() -> Bool {
        return editing
    }
}

// MARK: - Cell

class CoffeeListCell: TableAdapterCell {
    
    // MARK: Properties
    
    public var data: CoffeeListCellData?
    
    // MARK: Outlets
    
    @IBOutlet private weak var label0: UILabel?
    @IBOutlet private weak var label1: UILabel?
    @IBOutlet private weak var label2: UILabel?
    @IBOutlet private weak var label3: UILabel?
    
    @IBOutlet override var selectedView: UIView? { didSet { } }
    
    // MARK: Initialize
    
    override func awakeFromNib() {
        separator(hide: false)
    }
    
    override func fill(data: TableAdapterCellData?) {
        guard let data = data as? CoffeeListCellData else { return }
        self.data = data
        
        self.hideAnimation = data.touchAnimationHide
        //separator(hide: !data.separatorVisible)
        
        label0?.text = data.text0 ?? label0?.text
        label1?.text = data.text1 ?? label1?.text
        label2?.text = data.text2 ?? label2?.text
        label3?.text = data.text3 ?? label3?.text
        
    }
}

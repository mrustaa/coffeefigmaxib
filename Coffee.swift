//
//  Coffee.swift
//  DbTestSwift
//
//  Created by Рустам Мотыгуллин on 01.02.2021.
//

import Foundation

struct Coffee: Codable {
    
    let title: String
    let content: String
    
    let address: [CoffeeAddress]
    
    let url: String
    let imageURL: String
    let insta: String?
    
    let stuff: String
    let tags: [String]?
    
    let cappuccino: Int?
    let espresso: Int?
}

extension Coffee {
    init(data: Data) throws {
        self = try JSONDecoder().decode(Coffee.self, from: data)
    }
}

//
//  DbCoffee+CoreDataClass.swift
//  DbTestSwift
//
//  Created by Рустам Мотыгуллин on 01.02.2021.
//
//

import Foundation
import CoreData

@objc(DbCoffee)
public class DbCoffee: NSManagedObject {

}

extension DbCoffee {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<DbCoffee> {
        return NSFetchRequest<DbCoffee>(entityName: "DbCoffee")
    }
    
    @NSManaged public var cappuccino: Int64
    @NSManaged public var content: String?
    @NSManaged public var espresso: Int64
    
    @NSManaged public var url: String?
    @NSManaged public var imageURL: String?
    
    @NSManaged public var insta: String?
    @NSManaged public var instaUrlBad: Bool
    @NSManaged public var instaImg1: Data?
    @NSManaged public var instaImg2: Data?
    @NSManaged public var instaImg3: Data?
    
    @NSManaged public var stuff: String?
    @NSManaged public var tags: [String]?
    @NSManaged public var title: String?
    @NSManaged public var address: NSSet?
    @NSManaged public var response: DbCoffeeResponse?
    
}

// MARK: Generated accessors for address
extension DbCoffee {
    
    @objc(addAddressObject:)
    @NSManaged public func addToAddress(_ value: DbCoffeeAddress)
    
    @objc(removeAddressObject:)
    @NSManaged public func removeFromAddress(_ value: DbCoffeeAddress)
    
    @objc(addAddress:)
    @NSManaged public func addToAddress(_ values: NSSet)
    
    @objc(removeAddress:)
    @NSManaged public func removeFromAddress(_ values: NSSet)
    
}

extension DbCoffee : Identifiable {
    
}

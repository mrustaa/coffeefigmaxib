//
//  CoffeeResponse.swift
//  DbTestSwift
//
//  Created by Рустам Мотыгуллин on 01.02.2021.
//

import Foundation

struct CoffeeResponse: Codable {
    let new: [CoffeeNew]?
    let response: [Coffee]
}

extension CoffeeResponse {
    init(data: Data) throws {
        self = try JSONDecoder().decode(CoffeeResponse.self, from: data)
    }
}

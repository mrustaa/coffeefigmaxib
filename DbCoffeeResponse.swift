//
//  DbCoffeeResponse+CoreDataClass.swift
//  DbTestSwift
//
//  Created by Рустам Мотыгуллин on 01.02.2021.
//
//

import Foundation
import CoreData

@objc(DbCoffeeResponse)
public class DbCoffeeResponse: NSManagedObject {

}

extension DbCoffeeResponse {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<DbCoffeeResponse> {
        return NSFetchRequest<DbCoffeeResponse>(entityName: "DbCoffeeResponse")
    }
    
    @NSManaged public var new: NSSet?
    @NSManaged public var response: NSSet?
    
}

// MARK: Generated accessors for new
extension DbCoffeeResponse {
    
    @objc(addNewObject:)
    @NSManaged public func addToNew(_ value: DbCoffeeNew)
    
    @objc(removeNewObject:)
    @NSManaged public func removeFromNew(_ value: DbCoffeeNew)
    
    @objc(addNew:)
    @NSManaged public func addToNew(_ values: NSSet)
    
    @objc(removeNew:)
    @NSManaged public func removeFromNew(_ values: NSSet)
    
}

// MARK: Generated accessors for response
extension DbCoffeeResponse {
    
    @objc(addResponseObject:)
    @NSManaged public func addToResponse(_ value: DbCoffee)
    
    @objc(removeResponseObject:)
    @NSManaged public func removeFromResponse(_ value: DbCoffee)
    
    @objc(addResponse:)
    @NSManaged public func addToResponse(_ values: NSSet)
    
    @objc(removeResponse:)
    @NSManaged public func removeFromResponse(_ values: NSSet)
    
}

extension DbCoffeeResponse : Identifiable {
    
}

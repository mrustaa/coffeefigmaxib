//
//  CoffeeAddress.swift
//  DbTestSwift
//
//  Created by Рустам Мотыгуллин on 01.02.2021.
//

import Foundation

struct CoffeeAddress: Codable {
    let lat: Double
    let long: Double
    let street: String
    let time: String
}

extension CoffeeAddress {
    init(data: Data) throws {
        self = try JSONDecoder().decode(CoffeeAddress.self, from: data)
    }
}

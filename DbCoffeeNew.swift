//
//  DbCoffeeNew+CoreDataClass.swift
//  DbTestSwift
//
//  Created by Рустам Мотыгуллин on 01.02.2021.
//
//

import Foundation
import CoreData

@objc(DbCoffeeNew)
public class DbCoffeeNew: NSManagedObject {

}

extension DbCoffeeNew {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<DbCoffeeNew> {
        return NSFetchRequest<DbCoffeeNew>(entityName: "DbCoffeeNew")
    }
    
    @NSManaged public var street: String?
    @NSManaged public var title: String?
    @NSManaged public var response: DbCoffeeResponse?
    
}

extension DbCoffeeNew : Identifiable {
    
}
